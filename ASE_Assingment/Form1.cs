﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Assingment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private const int MAX = 256;      // max iterations
        private const double X = -2.025; // start value real
        private const double Y = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, changeClr;
        private static float xy;
        private Bitmap picture;
        private Graphics g1;
        private Cursor c1, c2;
        private HSBColor HSBCol = new HSBColor();
        Rectangle rec = new Rectangle(0, 0, 0, 0);
        Pen pen, pen1;
        bool _mousePressed;



       

        private void Form1_Load(object sender, EventArgs e)
        {

            init();
            start();

        }

        private void About_Click(object sender, EventArgs e)
        {
            MessageBox.Show("The MandleBrot Designed By Abd Bastola");
        }

        public struct HSBColor

        {

            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }


            public static Color FromHSB(HSBColor hsbcolor)
            {
                float r = hsbcolor.b;
                float g = hsbcolor.b;
                float b = hsbcolor.b;
                if (hsbcolor.s != 0)
                {
                    float max = hsbcolor.b;
                    float dif = hsbcolor.b * hsbcolor.s / 255f;
                    float min = hsbcolor.b - dif;

                    float h = hsbcolor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }

                if (changeClr == true)
                {
                    return Color.FromArgb
                    (
                        hsbcolor.a,
                        (int)100,
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 200)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))


                        );
                }

                return Color.FromArgb
                    (
                        hsbcolor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        private void initvalues() // reset start values
        {
            xstart = X;
            ystart = Y;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;

        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }

            return (float)j / (float)MAX;
        }

        private void mandelbrot()
        {
            int x, y;
            float h, b, alt = 0.0f;
            pen = new Pen(Color.Red, 2);
            action = false;
            //setCursor(c1);
            c1 = Cursors.WaitCursor;



            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                        HSBCol = new HSBColor(h * 255, 0.8f * 255, b * 255);
                        Color color = HSBColor.FromHSB(HSBCol);
                        pen = new Pen(color);
                        alt = h;
                    }
                    g1.DrawLine(pen, x, y, x + 1, y);
                }

            c2 = Cursors.Cross;
            action = true;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //put the bitmap on the window
            Graphics gform = e.Graphics;
            gform.DrawImage(picture, 0, 0, x1, y1);
            pen1 = new Pen(Color.White);
            if (_mousePressed == true)
            {
                if (rectangle)
                {
                    gform.DrawRectangle(pen1, rec);
                    rectangle = true;
                    //g1.(Color.White);
                    if (xs < xe)
                    {
                        if (ys < ye)
                        {
                            gform.DrawRectangle(pen1, xs, ys, (xe - xs), (ye - ys));
                            Invalidate();
                            //rectangle = false;
                        }

                        else gform.DrawRectangle(pen1, xs, ye, (xe - xs), (ys - ye));
                    }
                    else
                    {
                        if (ys < ye) gform.DrawRectangle(pen1, xe, ys, (xs - xe), (ye - ys));
                        else gform.DrawRectangle(pen1, xe, ye, (xs - xe), (ys - ye));
                    }
                }
                else if (_mousePressed == false)
                {
                    rectangle = false;
                }

            }
            else
            {
                gform.DrawImageUnscaled(picture, 0, 0);

            }
            if (rectangle == false)
            {
                Invalidate();
            }
        }

        public void init() // all instances will be prepared
        {

            x1 = pictureBox1.Width;
            y1 = pictureBox1.Height;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);


        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            pen = new Pen(Color.Red, 2);
            mandelbrot();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //e.consume();

            if (action)
            {

                xs = e.X;
                ys = e.Y;
                _mousePressed = true;



            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mousePressed)
            {
                //e.consume();
                if (action)
                {
                    //xe = e.getX();
                    xe = e.X;
                    //ye = e.getY();
                    ye = e.Y;
                    rectangle = true;
                    //repaint();                    
                    pictureBox1.Invalidate();
                }
            }
        }




        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;

                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;

                _mousePressed = false;
                Refresh();
                this.Invalidate();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "";
            save.Filter = "PNG(*.png)|*.png;|Bmp(*.bmp)|bmp;|Jpeg(*.jpg)|*.jpg";
            ImageFormat format = ImageFormat.Png;
            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(save.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                picture.Save(save.FileName, format);
                MessageBox.Show("File Saved");

            }
        }

        private void changeColourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            changeClr = true;
            mandelbrot();
            Refresh();
        }
        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
            pictureBox1.Update();

            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();

            rectangle = false;

            _mousePressed = false;
            Refresh();
            this.Invalidate();

        }



    }
}

